# AWS Lambda Node

The project source includes function code and supporting resources:

- `function` - A Node.js function.
- `template.yml` - An AWS CloudFormation template
- `1-create-bucket.sh`, `2-deploy.sh`, etc.