const AWSXRay = require('aws-xray-sdk-core')
const AWS = AWSXRay.captureAWS(require('aws-sdk'))

const lambda = new AWS.Lambda()

exports.handler = async function(event, context) {
  event.Records.forEach(record => {
    console.log(record.body)
  })
  return getAccountSettings()
}

var getAccountSettings = function(){
  return lambda.getAccountSettings().promise()
}

var serialize = function(object) {
  return JSON.stringify(object, null, 2)
}
